#!/bin/bash

grab_files ()  {
	if [ ! -d ./${device}/ ] ; then
		mkdir ./${device}/ || true
	fi
	if [ -f /opt/u-boot/bb-u-boot-${device}/sysfw.itb ] ; then
		cp -v /opt/u-boot/bb-u-boot-${device}/sysfw.itb ./${device}/
	fi
	if [ -f /opt/u-boot/bb-u-boot-${device}/tiboot3.bin ] ; then
		cp -v /opt/u-boot/bb-u-boot-${device}/tiboot3.bin ./${device}/
	fi
	if [ -f /opt/u-boot/bb-u-boot-${device}/tispl.bin ] ; then
		cp -v /opt/u-boot/bb-u-boot-${device}/tispl.bin ./${device}/
	fi
	if [ -f /opt/u-boot/bb-u-boot-${device}/u-boot.img ] ; then
		cp -v /opt/u-boot/bb-u-boot-${device}/u-boot.img ./${device}/
	fi
	dpkg-query --showformat='${Version}' --show bb-u-boot-${device} > ./${device}/version.txt
}

sudo apt update
sudo apt-get dist-upgrade -y
sudo apt-get install bb-u-boot-beagleboneai64 bb-u-boot-beagleplay bb-u-boot-beagley-ai bb-u-boot-pocketbeagle2 -y

device=beagleboneai64 ; grab_files
device=beagleplay ; grab_files
device=beagley-ai ; grab_files
device=pocketbeagle2 ; grab_files
